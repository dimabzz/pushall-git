**Install**
1.  Before installation, make sure that you have curl and jq packages installed.
2.  Copy post-receive to the repository directory in the hooks folder.
3.  Set git config values.

**Example**

```
ubuntu:/test.git$ git config hooks.pushall.id 1234
ubuntu:/test.git$ git config hooks.pushall.key 4r7y4hfh834f8g34gffj43hf78g4
ubuntu:/test.git$ git config hooks.pushall.type multicast
ubuntu:/test.git$ git config hooks.pushall.ttl 86400
ubuntu:/test.git$ git config hooks.pushall.uid 57689
ubuntu:/test.git$ git config hooks.pushall.uids 57689,34745
```

**Documentation**

[Full documentation](https://pushall.ru/blog/api)